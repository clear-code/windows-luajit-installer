---
title: インストール
---

# インストール

[最新のインストーラー][installer]をダウンロードして実行します。あとは
インストーラーの質問に答えていくとインストールが完了します。インストー
ル中に[MSYS2][msys2]をダウンロード・インストールするので10分ほどかかり
ます。

インストーラーはサイレント実行に対応しています。次のように`/S`オプショ
ンをつけて実行することでサイレントインストールできます。

```text
.\luajit-installer-{{ site.version }}.exe /S
```

`/D`オプションでインストールフォルダーを変更できます。デフォルトのイン
ストールフォルダーは`C:\luajit`なので、違う場所にインストールしたい場
合は指定してください。次の例は`E:\applications\luajit`にインストールし
ています。

```text
.\luajit-installer-{{ site.version }}.exe /S /DE:\applications\luajit
```

インストール後の使い方は[使い方][usage]を参照してください。

アンインストール方法は[アンインストール][uninstall]を参照してください。

[installer]:{{ site.installer_url }}

[msys2]:https://www.msys2.org/

[usage]:../usage/

[uninstall]:../uninstall/
