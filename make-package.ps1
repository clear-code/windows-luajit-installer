$MSYS2_ARCHITECTURE = "x86_64"
$MSYS2_INSTALLER_TAG = "2020-07-20"
$MSYS2_INSTALLER_VERSION = "20200720"
$MSYS2_INSTALLER_NAME = "msys2-base-${MSYS2_ARCHITECTURE}-${MSYS2_INSTALLER_VERSION}.sfx.exe"
$MSYS2_INSTALLER_URL_BASE = "https://github.com/msys2/msys2-installer/releases/download/${MSYS2_INSTALLER_TAG}"
$MSYS2_MAKEPKG_PACKAGES = "base-devel git mingw-w64-x86_64-toolchain"
$MSYS2_MAKEPKG_WORKAROUND_PACKAGES = "unzip winpty mingw-w64-x86_64-luajit"
$MSYS2_ROOT = "msys64"

Invoke-WebRequest `
  -Uri ${MSYS2_INSTALLER_URL_BASE}/${MSYS2_INSTALLER_NAME} `
  -OutFile ${MSYS2_INSTALLER_NAME}

& .\${MSYS2_INSTALLER_NAME} -y

& ${MSYS2_ROOT}\msys2_shell.cmd `
  -defterm `
  -here `
  -msys2 `
  -no-start `
  -c "pacman --sync --refresh --sysupgrade --sysupgrade --noconfirm"
taskkill /F /FI "MODULES eq msys-2.0.dll"

& ${MSYS2_ROOT}\msys2_shell.cmd `
  -defterm `
  -here `
  -msys2 `
  -no-start `
  -c "pacman --sync --sysupgrade --sysupgrade --noconfirm"
taskkill /F /FI "MODULES eq msys-2.0.dll"

& ${MSYS2_ROOT}\msys2_shell.cmd `
  -defterm `
  -here `
  -msys2 `
  -no-start `
  -c "pacman --needed --noconfirm --sync mingw-w64-x86_64-nsis"

& ${MSYS2_ROOT}\msys2_shell.cmd `
  -defterm `
  -here `
  -msys2 `
  -no-start `
  -c "pacman --needed --noconfirm --sync ${MSYS2_MAKEPKG_PACKAGES}"

& ${MSYS2_ROOT}\msys2_shell.cmd `
  -defterm `
  -here `
  -msys2 `
  -no-start `
  -c "pacman --needed --noconfirm --sync ${MSYS2_MAKEPKG_WORKAROUND_PACKAGES}"

& ${MSYS2_ROOT}\msys2_shell.cmd `
  -defterm `
  -here `
  -mingw64 `
  -no-start `
  -c "make"

cd mingw-w64-luajit-luarocks
& ..\${MSYS2_ROOT}\msys2_shell.cmd `
  -defterm `
  -here `
  -msys2 `
  -no-start `
  -c "env MINGW_INSTALLS=mingw64 makepkg-mingw -sLf"
cd ..

Copy-Item *\*.pkg.tar.zst .\
& ${MSYS2_ROOT}\msys2_shell.cmd `
  -defterm `
  -here `
  -mingw64 `
  -no-start `
  -c "makensis luajit-installer.nsi"

echo Done
